import {Banner} from "../components/Navbar/Banner";
import {Buscador} from "../components/Buscador/Buscador";
import {ListadoPersonajes} from "../components/Personajes/ListadoPersonajes";
import {useState} from "react";

export function Home() {
	let [buscador, setBuscador] = useState(null);
	return (
		<div>
			<Banner />
			<div className="px-5">
				<div className="row">
					<h2 className="py-4">Personajes destacados</h2>
					<Buscador valor={buscador} onBuscar={setBuscador} />
					<ListadoPersonajes buscar={buscador} />
				</div>
			</div>
		</div>
	);
}
