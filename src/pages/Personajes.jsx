import {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import axios from "axios";
import {BannerDetallePersonaje} from "../components/Personajes/BannerDetallePersonaje";
import {ListaEpisodio} from "../components/Episodio/ListaEpisodio";

export function Personajes() {
	let {personajeId} = useParams();
	let [personaje, setPersonaje] = useState(null);
	let [episodios, setEpisodios] = useState(null);

	useEffect(() => {
		axios
			.get(`https://rickandmortyapi.com/api/character/${personajeId}`)
			.then((respuesta) => {
				setPersonaje(respuesta.data);
			});
	}, []);

	useEffect(() => {
		if (personaje) {
			let peticionesEpisodios = personaje.episode.map((episodio) => {
				return axios.get(episodio);
			});

			Promise.all(peticionesEpisodios).then((respuestas) => {
				setEpisodios(respuestas);
			});
		}
	}, [personaje]);

	return (
		<div
			className="container"
			style={{marginTop: "100px", marginBottom: "20px"}}
		>
			<div className="row">
				{personaje ? (
					<div className="container">
						<BannerDetallePersonaje {...personaje} />

						<h2 className="text-center">Episodios {personaje.name} </h2>
						{episodios ? (
							<ListaEpisodio episodios={episodios} />
						) : (
							<div>Cargando episodios...</div>
						)}
					</div>
				) : (
					<div>Cargando...</div>
				)}
			</div>
		</div>
	);
}
