export function Footer() {
	return (
		<div className="navbar-dark bg-dark fixed-bottom">
			<h6 className="text-center text-white py-3 mb-0">
				Proyecto Rick and Morty por Juan Pablo Lara Angulo CAR IV &copy; 2022
			</h6>
		</div>
	);
}
