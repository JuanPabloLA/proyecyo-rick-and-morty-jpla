export function Buscador({valor, onBuscar}) {
	return (
		<div className="d-flex justify-content-end mb-3">
			<form className="col-3 d-flex">
				<input
					className="form-control me-2"
					type="search"
					placeholder="Buscar personaje"
					aria-label="Buscar personaje"
					value={valor}
					onChange={(evento) => onBuscar(evento.target.value)}
				/>
				<button className="btn btn-outline-success" type="submit">
					Buscar
				</button>
			</form>
		</div>
	);
}
