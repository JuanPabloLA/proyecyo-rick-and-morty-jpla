import bannerImg from "../../assets/img/RickAndMortyBanner.jpg";

export function Banner() {
	return (
		<div style={{marginTop: "100px"}}>
			<img
				src={bannerImg}
				className="card-img-bottom"
				alt="Banner Rick and Morty"
			/>
		</div>
	);
}
