import {Link} from "react-router-dom";
import logo from "../../assets/img/rick-and-morty-lg.png";

export function Header() {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
			<div className="container-fluid">
				<Link className="navbar-brand" to="/">
					<img src={logo} alt="" width="30%" height="auto" />
				</Link>
				<div className="navbar-nav" id="navbarText">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item">
							<Link className="nav-link active" aria-current="page" to="/">
								Home
							</Link>
						</li>
						{/* <li className="nav-item">
							<Link className="nav-link" to="/Personajes">
								Personajes
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" to="#">
								Pricing
							</Link>
						</li> */}
					</ul>
				</div>
			</div>
		</nav>
	);
}
