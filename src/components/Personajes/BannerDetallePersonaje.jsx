function getEstilosStatus(status) {
	let color = "green";

	if (status === "unknown") {
		color = "gray";
	}

	if (status === "Dead") {
		color = "red";
	}
	const estiloCirculo = {
		width: "10px",
		height: "10px",
		display: "inline-block",
		backgroundColor: color,
		borderRadius: "50%",
		marginRight: "5px",
	};
	return estiloCirculo;
}

export function BannerDetallePersonaje({
	id,
	name,
	status,
	species,
	location,
	origin,
	image,
}) {
	return (
		<div className="col-md-6 offset-md-3 my-3" style={{maxWidth: "540px"}}>
			<div className="row g-0 bg-ligth" style={{borderRadius: "5px"}}>
				<div className="col-md-6">
					<img
						src={image}
						className="img-fluid rounded-start rounded-end"
						alt={name}
						style={{width: "100%", objectFit: "cover"}}
					/>
				</div>
				<div className="col-md-6 d-flex align-items-center">
					<div className="card-body text-dark">
						{/* <Link to={`//${id}`}> */}
						<h4 className="card-title mb-0">{name}</h4>
						{/* </Link> */}
						<p>
							<span style={getEstilosStatus(status)}></span>
							{status} - {species}
						</p>
						<p className="mb-0 text-muted">Last know location:</p>
						<p>{location?.name}</p>
						<p className="mb-0 text-muted">Origin:</p>
						<p>{origin?.name}</p>
					</div>
				</div>
			</div>
		</div>
	);
}
