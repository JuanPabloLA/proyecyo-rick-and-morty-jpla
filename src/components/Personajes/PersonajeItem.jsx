import {Link} from "react-router-dom";

function getEstilosStatus(status) {
	let color = "green";

	if (status === "unknown") {
		color = "gray";
	}

	if (status === "Dead") {
		color = "red";
	}
	const estiloCirculo = {
		width: "10px",
		height: "10px",
		display: "inline-block",
		backgroundColor: color,
		borderRadius: "50%",
		marginRight: "5px",
	};
	return estiloCirculo;
}

export function PersonajeItem({
	id,
	name,
	status,
	species,
	location,
	origin,
	image,
}) {
	return (
		<div className="col-4 my-2">
			<div className="card mb-3" style={{maxWidth: "540px"}}>
				<div className="row g-0 bg-dark" style={{borderRadius: "5px"}}>
					<div className="col-md-4">
						<img
							src={image}
							className="img-fluid rounded-start"
							alt={name}
							style={{height: "100%", objectFit: "cover"}}
						/>
					</div>
					<div className="col-md-8">
						<div className="card-body text-white">
							<Link to={`/personajes/${id}`}>
								<h5 className="card-title mb-0">{name}</h5>
							</Link>
							<p>
								<span style={getEstilosStatus(status)}></span>
								{status} - {species}
							</p>
							<p className="mb-0 text-muted">Last know location:</p>
							<p>{location?.name}</p>
							<p className="mb-0 text-muted">Origin:</p>
							<p>{origin?.name}</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}
