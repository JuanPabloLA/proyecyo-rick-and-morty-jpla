import {Episodio} from "./Episodio";

export function ListaEpisodio({episodios}) {
	return (
		<div className="container p-0">
			<div className="row ml-auto mr-auto pb-5">
				{episodios.map((episodio) => (
					<Episodio key={episodio.data.id} {...episodio.data} />
				))}
			</div>
		</div>
	);
}
