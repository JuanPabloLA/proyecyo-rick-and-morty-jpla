export function Episodio({name, air_date, episode}) {
	return (
		<div
			className="col-4"
			style={{background: "#DBDBDB", borderRadius: "10px"}}
		>
			<div
				className="card text-dark bg-light m-3 p-0"
				style={{maxWidth: "350px"}}
			>
				<div className="card-header">{air_date}</div>
				<div className="card-body">
					<h5 className="card-title">{name}</h5>
					<p className="card-text">{episode}</p>
				</div>
			</div>
		</div>
	);
}
