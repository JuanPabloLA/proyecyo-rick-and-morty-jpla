import {useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {Routes, Route, Link} from "react-router-dom";
import {Home} from "./pages/Home";
import {Personajes} from "./pages/Personajes";
import {Header} from "./components/Navbar/Header";
import {Footer} from "./components/footer/Footer";

function App() {
	return (
		<div className="App">
			<Header />
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/personajes/:personajeId" element={<Personajes />} />
			</Routes>
			<Footer />
		</div>
	);
}

export default App;
